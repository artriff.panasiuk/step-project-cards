import sendRequest from "./../function/sendRequest.js";
const token = localStorage.getItem("token");

import CreateCardModal from "./CreateCardModal.js";
import draqAndDropFunc from "./../function/draqAndDropFunc.js";
const container = document.getElementById("section-cards");

class Visit {
  constructor(cardData) {
    this.id = cardData.id;
    this.fullName = cardData.fullName;
    this.description = cardData.description;
    this.priority = cardData.priority;
    this.doctor = cardData.doctor;
    this.age = cardData.age;
    this.title = cardData.title;
    this.lastVisit = cardData.lastVisit;
    this.diseases = cardData.diseases;
    this.weight = cardData.weight;
    this.presure = cardData.presure;
    this.status = cardData.status;
  }

  createCard() {
    //створюю картку
    const card = document.createElement("div");
    card.classList.add("card");
    card.classList.add("mb-2");
    card.setAttribute("id", `${this.id}`);

    // додаю кнопки закриття і редагування картки
    const buttonsWrapper = document.createElement("div");
    buttonsWrapper.classList.add("buttons-wrapper");
    buttonsWrapper.innerHTML = `<div> <div class="btn-closed" disabled aria-label="Delite">
                                        <img id="del${this.id}" src="./img/iconClose.png" alt="">
                                    </div>
                                    <div id="edit${this.id}" class="btn-edit" disabled aria-label="Edit">
                                        <img src="./img/iconEdit.png" alt="">
                                    </div> </div>`;
    const status = document.createElement("div");
    status.classList.add("status");
    status.innerHTML = `
                                <div class="input-group mb-1">
                                <div class="input-group-text">
                                <label class="status-label" for="visitStatus">Done</label>
                                <input id="status${
                                  this.id
                                }" class="form-check-input mt-0" type="checkbox" value="${
      this.status
    }" ${this.status === "Done" ? "checked" : "unchecked"}>
                                  </div>
                                </div>
                              </div>

    `;
    card.append(buttonsWrapper);
    buttonsWrapper.prepend(status);

    // Створюю тіло інформації картки
    const cardBody = document.createElement("div");
    cardBody.classList.add("card-body");
    cardBody.innerHTML = `  <h5 class="card-title">${this.fullName}</h5>
                                <p class="card-text">${this.doctor}</p>`;
    card.append(cardBody);
    const cardInfo = document.createElement("div");
    cardInfo.classList.add("card-info");
    card.append(cardInfo);
    //Створюю кнопку 'Показати більше'
    const showMoreBtn = document.createElement("button");
    showMoreBtn.classList.add("btn");
    showMoreBtn.classList.add("btn-primary");
    showMoreBtn.classList.add("show_more");
    showMoreBtn.innerText = "Показати більше";
    showMoreBtn.setAttribute("id", `show${this.id}`);
    card.append(showMoreBtn);
    container.append(card);
  }

  addVisitStatus() {
    const visitStatus = document.getElementById(`status${this.id}`);
    const cardId = visitStatus.id.slice(6);

    visitStatus.addEventListener("click", function (e) {
      sendRequest(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, "GET", {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      }).then((cardData) => {
        const card = { ...cardData, status: this.checked ? "Done" : "Open" };
        sendRequest(
          `https://ajax.test-danit.com/api/v2/cards/${cardId}`,
          "PUT",
          {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify(card),
          }
        );
        let arrayBase = JSON.parse(localStorage.getItem("localBase"));
        arrayBase.forEach((client, ind) => {
          if (client.id === card.id) {
            arrayBase[ind].status = card.status;
          }
        });
        localStorage.setItem("localBase", JSON.stringify(arrayBase));
      });
    });
  }

  deleteCard() {
    const delBtn = document.getElementById(`del${this.id}`);
    const card = document.getElementById(`${this.id}`);
    delBtn.addEventListener("click", () => {
      sendRequest(
        `https://ajax.test-danit.com/api/v2/cards/${this.id}`,
        "DELETE",
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      ).then(() => {
        card.classList.add("invisible");
        let arrayBase = JSON.parse(localStorage.getItem("localBase"));
        const newArray = arrayBase.filter((card) => card.id !== this.id);
        localStorage.setItem("localBase", JSON.stringify(newArray));
        draqAndDropFunc();
      });
    });
  }

  editCard() {
    const editBtn = document.getElementById(`edit${this.id}`);
    editBtn.addEventListener("click", () => {
      sendRequest(
        `https://ajax.test-danit.com/api/v2/cards/${this.id}`,
        "GET",
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
        }
      ).then((userCardData) => {
        const editUserCreateCard = new CreateCardModal(
          token,
          ".modal-create-card ",
          "edit",
          userCardData
        );
        editUserCreateCard.showModalCreateCard();
      });
    });
  }

  showMoreInfo() {
    const showBtn = document.getElementById(`show${this.id}`);
    showBtn.addEventListener("click", () => {
      showBtn.setAttribute("disabled", true);
      const info = showBtn.previousElementSibling;
      info.insertAdjacentHTML(
        "afterbegin",
        `
                    <p class="${this.age ? "info-team" : "invisible"}">Вік: ${
          this.age
        }</p>
                    <p class="${
                      this.description ? "info-team" : "invisible"
                    }">Опис візиту: ${this.description}</p>
                    <p class="${
                      this.priority ? "info-team" : "invisible"
                    }">Терміновість: ${this.priority}</p>
                    <p class="${
                      this.title ? "info-team" : "invisible"
                    }">Процедура: ${this.title}</p>
                    <p class="${
                      this.lastVisit ? "info-team" : "invisible"
                    }">Останній візит: ${this.lastVisit}</p>
                    <p class="${
                      this.diseases ? "info-team" : "invisible"
                    }">Хвороби: ${this.diseases}</p>
                    <p class="${
                      this.weight ? "info-team" : "invisible"
                    }">iндекс маси тіла: ${this.weight}</p>
                    <p class="${
                      this.presure ? "info-team" : "invisible"
                    }">Тиск: ${this.presure}</p>
                
              `
      );
    });
  }
}

export default Visit;
