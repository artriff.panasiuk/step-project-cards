function draqAndDropFunc(){
  const ArrayCardElements = [...document.querySelector('.cards-wrapper').children];
  ArrayCardElements.forEach((child,index) => {
    child.classList.add('item');
    child.setAttribute('id',`item${index}`)
  });
  $(function() {
    $(".item").draggable({
      start : function (event , ui){
        const target = document.getElementById(this.id);
        target.style.zIndex=100;
      },
      stop : function (event , ui){
          let nowPosition = new Object();
          let newPosition = new Array();
  
          for (let i = 1; i < ArrayCardElements.length; i++) {
             let positionData = getPosition(i);
              nowPosition = {'name':'item'+i,'position':positionData};
  
               newPosition.push(nowPosition);
          }
          newPosition.sort(function(a,b){
                  if( a['position'] > b['position'] ) return -1;
                  if( a['position'] < b['position'] ) return 1;
                  return 0;
          });
  
          let number = 0;
          for (let i = newPosition.length; i--; ) {
              const tmpItem = document.getElementById(newPosition[i].name);
              tmpItem.style.order = number;
              tmpItem.style.left = 0;
              tmpItem.style.top = 0;
  
              number ++;
          }
  
        const target = document.getElementById(this.id);
        target.style.zIndex=0;
      }
  
    });
  
    function getPosition(item){
      const tmpItem = document.getElementById('item'+item);
      return tmpItem.offsetLeft;
    }
  });
}
export default draqAndDropFunc;
