"use strict";
import AvtorizationModal from "./modules/modalForm/AvtorizationModal.js";
import CreateCardModal from "./modules/modalForm/CreateCardModal.js";
import draqAndDropFunc from "./modules/function/draqAndDropFunc.js";
import UserCardRender from "./modules/modalForm/UserCardRender.js";
const btnAuntification = document.querySelector(".btn-auntification");
const btnCreateCard = document.querySelector(".btn__create-card");
const token = localStorage.getItem("token");

//Рендер через локал сторедж
const cardsWrapper = document.querySelector(".cards-wrapper");
let arrayBase = JSON.parse(localStorage.getItem("localBase"));
if (arrayBase || (arrayBase && !!arrayBase.length)) {
  arrayBase.forEach((client) => {
    const card = new UserCardRender(client);
    card.createCard();
    card.showMoreInfo();
  });
  draqAndDropFunc();
} else {
  cardsWrapper.innerHTML =
    "<h1 class='message-nocard mt-5 ps-2 pe-2' style='color:white; background:red; margin: 0 auto;'>No items have been added</h1>";
}

const modalAvtorization = new AvtorizationModal(".modal-authorization");

btnAuntification.addEventListener("click", () => {
  modalAvtorization.showModalAvtorization();
});

btnCreateCard.addEventListener("click", () => {
  const modalCreateCard = new CreateCardModal(
    token,
    ".modal-create-card",
    "create",
    null
  );
  modalCreateCard.showModalCreateCard();
});

document.querySelector(".btn-search").addEventListener("click", () => {
  const priorityValue = document.querySelector(".search-priority").value;
  const statusValue = document.querySelector(".status-search").value;
  const descValue = document.querySelector(".search-desc").value;
  const arrayBase = JSON.parse(localStorage.getItem("localBase"));
  let resoultArray;
  if (statusValue !== "All") {
    if (descValue) {
      resoultArray = arrayBase.filter((card) => {
        return (
          (card.title.toLowerCase().includes(descValue.toLowerCase()) ||
            card.description.toLowerCase().includes(descValue.toLowerCase())) &&
          card.priority === priorityValue &&
          card.status === statusValue
        );
      });
    } else {
      resoultArray = arrayBase.filter(
        (card) => card.priority === priorityValue && card.status === statusValue
      );
    }
  } else {
    if (descValue) {
      resoultArray = arrayBase.filter((card) => {
        return (
          (card.title.toLowerCase().includes(descValue.toLowerCase()) ||
            card.description.toLowerCase().includes(descValue.toLowerCase())) &&
          card.priority === priorityValue
        );
      });
    } else {
      resoultArray = arrayBase.filter(
        (card) => card.priority === priorityValue
      );
    }
  }

  document.querySelector(".cards-wrapper").innerText = "";
  resoultArray.forEach((client) => {
    const card = new UserCardRender(client);
    card.createCard();
    card.showMoreInfo();
    if (modalAvtorization.getToken()) {
      card.deleteCard();
      card.addVisitStatus();
      card.editCard();
    }
  });
  draqAndDropFunc();
});
